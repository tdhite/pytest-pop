import os
from unittest import mock

import pytest
import yaml

try:
    import acct.conf

    HAS_ACCT = True
except ImportError:
    HAS_ACCT = False

try:
    import idem.conf

    HAS_IDEM = True
except ImportError:
    HAS_IDEM = False

ACCT_KEY = "BjN1KRcfbWPO6G12328vA-3omHGGUxu9z3NvPV-MxOI="
ACCT_FILE = os.path.join(os.path.dirname(__file__), "acct_profile")
ENC_ACCT_FILE = os.path.join(os.path.dirname(__file__), "acct_profile.fernet")


@pytest.fixture(scope="session", name="hub")
def my_hub(hub):
    # Get the account information from environment variables
    with mock.patch.dict(
        "os.environ", {"ACCT_KEY": ACCT_KEY, "ACCT_FILE": ENC_ACCT_FILE}
    ):
        with mock.patch("sys.argv", ["pytest_pop"]):
            hub.pop.config.load(["acct"], "acct", parse_cli=False)

    yield hub


@pytest.fixture(scope="session")
def acct_subs():
    return ["test_provider"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_profile"


@pytest.mark.skipif(not HAS_ACCT, reason="acct is not installed for this test")
def test_missing_dynes(hub):
    """
    The ctx fixture hasn't been used yet, no idem on hub
    """
    assert not hasattr(hub, "acct")
    assert not hasattr(hub, "idem")
    assert not hasattr(hub, "exec")
    assert not hasattr(hub, "states")
    assert not hasattr(hub, "tool")


@pytest.mark.skipif(not HAS_ACCT, reason="acct is not installed for this test")
def test_ctx(ctx):
    assert "run_name" in ctx
    assert "test" in ctx
    assert hasattr(ctx, "acct")
    assert "acct" in ctx
    assert ctx["acct"]
    assert ctx.acct
    with open(ACCT_FILE) as fh_:
        profiles = yaml.safe_load(fh_)
    assert profiles["test_provider"]["test_profile"] == ctx.acct


@pytest.mark.skipif(not HAS_IDEM, reason="idem is not installed for this test")
def test_dynes(hub):
    assert hasattr(hub, "acct")
    assert hasattr(hub, "idem")
    assert hasattr(hub, "exec")
    assert hasattr(hub, "states")
    assert hasattr(hub, "tool")
