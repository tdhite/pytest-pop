import pytest


@staticmethod
@pytest.fixture(scope="module", name="hub")
def my_hub(hub):
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    yield hub


def test_hub_contract(mock_attr_hub, hub):
    assert hub.mods.testing.echo("foo") == "contract foo"
    assert mock_attr_hub.mods.testing.echo("foo") == "contract foo"

    hub.VAL = {}
    assert "Mock" in str(mock_attr_hub.OPT)
