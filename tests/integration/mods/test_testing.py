from unittest.mock import NonCallableMock
from unittest.mock import sentinel

import pytest
from pop.hub import Hub

import pytest_pop.mods.testing as testing


@pytest.fixture(scope="module", name="hub")
def my_hub(hub):
    hub.pop.sub.add("tests.mods")
    yield hub


def test_hub_storage(hub, lazy_hub):
    assert lazy_hub._hub() is hub
    assert lazy_hub._lazy_hub() is lazy_hub
    assert lazy_hub._LazyPop__lut[hub] is lazy_hub

    assert lazy_hub.pop._hub() is hub


def test_lazy(mock_hub):
    assert len(mock_hub._LazyPop__lut) == 3
    mock_hub.mods
    assert len(mock_hub._LazyPop__lut) == 4
    mock_hub.mods.testing
    assert len(mock_hub._LazyPop__lut) == 5
    mock_hub.mods.testing.echo
    assert len(mock_hub._LazyPop__lut) == 6


def test_reset(mock_hub):
    mtesting = mock_hub.mods.testing
    echo = mock_hub.mods.testing.echo
    noparam = mock_hub.mods.testing.noparam
    assert len(mock_hub._LazyPop__lut) == 7

    mock_hub.mods.testing._reset()

    assert len(mock_hub._LazyPop__lut) == 5
    assert mock_hub.mods.testing is mtesting
    assert mock_hub.mods.testing.echo is not echo
    assert mock_hub.mods.testing.noparam is not noparam


def test_recursive_reset(mock_hub):
    mtesting = mock_hub.mods.testing
    mtest = mock_hub.mods.test
    assert len(mock_hub._LazyPop__lut) == 6
    mock_hub.mods.testing.echo
    mock_hub.mods.test.ping
    assert len(mock_hub._LazyPop__lut) == 8

    mock_hub.mods._reset()
    assert len(mock_hub._LazyPop__lut) == 4
    assert mock_hub.mods.testing is not mtesting
    assert mock_hub.mods.test is not mtest


def test_reset_assigment(mock_hub):
    aosnthu = object()
    mock_hub.mods.aosnthu = aosnthu
    mock_hub.mods._reset()

    assert not hasattr(mock_hub.mods, "aosnthu")


def test_in_dict(lazy_hub):
    # for autocompletion, all attributes should exist, even if it hasn't been called.
    assert "mods" in lazy_hub.__dict__


def test_duplicate_object(hub):
    hub.test_val = sentinel.test_val
    hub.mods.test_val = sentinel.test_val
    hub.mods.testing.test_val = sentinel.test_val

    l_hub = testing._LazyPop(hub)

    assert isinstance(l_hub.test_val, NonCallableMock)
    assert l_hub.test_val is l_hub.mods.test_val
    assert l_hub.mods.test_val is l_hub.mods.testing.test_val


def test_duplicate_hub(hub):
    hub.hub = hub
    hub.mods.hub = hub
    hub.mods.foo.hub = hub

    l_hub = testing._LazyPop(hub)

    assert l_hub.hub is l_hub
    assert l_hub.mods.hub is l_hub
    assert l_hub.mods.foo.hub is l_hub


def test_recursive_subs(hub):
    hub.pop.sub.add("tests.mods.nest", sub=hub.mods)
    l_hub = testing._LazyPop(hub)

    assert hub.mods.nest.basic.ret_true()

    with pytest.raises(NotImplementedError):
        l_hub.mods.nest.basic.ret_true()


def test_var_exists_enforcement(hub):
    hub.FOO = "foo"
    hub.mods.FOO = "foo"
    hub.mods.testing.FOO = "foo"

    l_hub = testing._LazyPop(hub)

    for _ in (l_hub, l_hub.mods, l_hub.mods.testing):
        with pytest.raises(AttributeError):
            l_hub.BAZ


def test_recursive_get(hub):
    assert hub.mods
    l_hub = testing._LazyPop(hub)

    result = getattr(l_hub, "mods.foo")
    assert result is l_hub.mods.foo


def test_find_subs():
    hub = Hub()
    hub.pop.sub.add("tests.mods")
    l_hub = testing._LazyPop(hub)

    subs = l_hub._find_subs()
    assert len(subs) == 2
    assert getattr(hub, subs[0][0]) is subs[0][1]

    # test nested subs
    hub.pop.sub.add(dyne_name="dn1")
    hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
    l_hub = testing._LazyPop(hub)
    subs = l_hub._find_subs()
    assert len(subs) == 3
    assert len(subs[2][0].split(".")) == 1
    assert getattr(hub, subs[2][0]) is subs[2][1]


def test_resolve_this(mock_hub):
    from tests.mods.test import this, double_underscore

    assert mock_hub.mods.test.ping() is this(mock_hub)
    double_underscore(mock_hub)


def test_containst(mock_hub):
    # testing should exist on mods
    assert "testing" in mock_hub.mods
    # echo should also exist on mods.testing
    assert "echo" in mock_hub.mods.testing
    # echoes should NOT exist on mods.testing
    assert "echoes" not in mock_hub.mods.testing
